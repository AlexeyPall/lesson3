
import subprocess
import sys

PATH_TO_PROJECT = '/home/alex/projects/pycharm/ASRlesson030'
PATH_TO_DATA_TEST = '/home/alex/lessons/ASR/decoder/test_data/data/myDaNet_etalon_test/test'
PATH_TO_DATA_ETALONS = '/home/alex/lessons/ASR/decoder/test_data/data/myDaNet_etalon_test/etalon'
PATH_TO_MFCC_CALC = '/home/alex/projects/ASR/kaldi/src/featbin'
MFCC_CALC_EXEC = 'compute-mfcc-feats'

PATH_TO_DATA_DIGITS = '/home/alex/lessons/ASR/decoder/test_data/data/myDigits_etalon_test'

def create_da_net_etalon_scp_file(fileout_name, base_path,
                                  da_file_num, net_file_num):

    fileout = open(fileout_name, 'w')

    # if da_file_num:
    for i in range(1, 6):
        fileout.write('da_' + str(i).zfill(2) + '.wav ' +
                      base_path + '/da_' + str(i).zfill(2) + '.wav' + '\n')

    # if net_file_num:
    for i in range(1, 6):
        fileout.write('net_' + str(i).zfill(2) + '.wav ' +
                      base_path + '/net_' + str(i).zfill(2) + '.wav' + '\n')


def create_da_net_test_scp_file(fileout_name, base_path,
                                da_max_file_num, net_max_file_num):
    """ Max da: 19; Max net: 21 """

    fileout = open(fileout_name, 'w')

    ''' Add Da '''
    for i in range(6, da_max_file_num + 1):
        fileout.write('da_' + str(i).zfill(2) + '.wav ' +
                      base_path + '/da_' + str(i).zfill(2) + '.wav' + '\n')

    ''' Add NET '''
    for i in range(6, net_max_file_num + 1):
        fileout.write('net_' + str(i).zfill(2) + '.wav ' +
                      base_path + '/net_' + str(i).zfill(2) + '.wav' + '\n')


def create_mfcc_features(path_to_scp, path_to_result):

    proc = subprocess.Popen([PATH_TO_MFCC_CALC + '/' + MFCC_CALC_EXEC,
                             'scp:' + path_to_scp,
                             'ark,t:' + path_to_result],
                            stdout=sys.stdout,
                            stderr=sys.stderr
                            )

    proc.wait()


def create_digits_etalon_scp_file(fileout_name, base_path):

    fileout = open(fileout_name, 'w')

    for i in range(10):
        fileout.write(str(i) + '_1.wav ' +
                      base_path + '/etalon/' + str(i) + '_1.wav' + '\n')


def create_digits_test_scp_file(fileout_name, base_path):

    fileout = open(fileout_name, 'w')

    for i in range(10):
        for j in range(2, 6):
            fileout.write(str(i) + '_' + str(j) + '.wav ' +
                          base_path + '/test/' + str(i) + '_' + str(j) + '.wav' + '\n')

# TEST 1
#
# ''' Create Da Net etalons '''
# path_to_scp_with_da_net = PATH_TO_PROJECT + '/data/da_net_etalons.scp'
# path_to_mfcc_with_da_net = PATH_TO_PROJECT + '/data/da_net_etalons.txtftr'
#
# create_da_net_etalon_scp_file(path_to_scp_with_da_net, PATH_TO_DATA_ETALONS,
#                               2, 2)
# create_mfcc_features(path_to_scp_with_da_net, path_to_mfcc_with_da_net)
#
# ''' Create test with etalon DA '''
# path_to_scp_with_da_net = PATH_TO_PROJECT + '/data/da_etalon_test.scp'
# path_to_mfcc_with_da_net = PATH_TO_PROJECT + '/data/da_etalon_test.txtftr'
#
# create_da_net_etalon_scp_file(path_to_scp_with_da_net, PATH_TO_DATA_ETALONS,
#                               1, 0)
# create_mfcc_features(path_to_scp_with_da_net, path_to_mfcc_with_da_net)
#
# ''' Create test with etalon NET '''
# path_to_scp_with_da_net = PATH_TO_PROJECT + '/data/net_etalon_test.scp'
# path_to_mfcc_with_da_net = PATH_TO_PROJECT + '/data/net_etalon_test.txtftr'
#
# create_da_net_etalon_scp_file(path_to_scp_with_da_net, PATH_TO_DATA_ETALONS,
#                               0, 1)
# create_mfcc_features(path_to_scp_with_da_net, path_to_mfcc_with_da_net)
#
# ''' Create Da Net test set '''
# path_to_scp_with_da_net = PATH_TO_PROJECT + '/data/da_net_test.scp'
# path_to_mfcc_with_da_net = PATH_TO_PROJECT + '/data/da_net_test.txtftr'
#
# create_da_net_test_scp_file(path_to_scp_with_da_net, PATH_TO_DATA_TEST,
#                             19, 21)
# create_mfcc_features(path_to_scp_with_da_net, path_to_mfcc_with_da_net,)


# TEST 2 - digits

''' Create digits etalons '''
path_to_scp_with_digits = PATH_TO_PROJECT + '/data/digits_etalons.scp'
path_to_mfcc_with_digits = PATH_TO_PROJECT + '/data/digits_etalons.txtftr'

create_digits_etalon_scp_file(path_to_scp_with_digits, PATH_TO_DATA_DIGITS)
create_mfcc_features(path_to_scp_with_digits, path_to_mfcc_with_digits)

''' Create digits test '''
path_to_scp_with_digits = PATH_TO_PROJECT + '/data/digits_test.scp'
path_to_mfcc_with_digits = PATH_TO_PROJECT + '/data/digits_test.txtftr'

create_digits_test_scp_file(path_to_scp_with_digits, PATH_TO_DATA_DIGITS)
create_mfcc_features(path_to_scp_with_digits, path_to_mfcc_with_digits)