#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import sys

import numpy as np

import FtrFile


def euclidean_distance(vec1, vec2):
    return np.sqrt(np.sum(np.power(vec1 - vec2, 2)))


#########################################
# State, Graph, etc...
#########################################

class State:
    def __init__(self, ftr, idx):  # idx is for debug purposes
        self.ftr = ftr
        self.word = None
        self.isFinal = False
        self.nextStates = []
        self.idx = idx


def print_state(state):
    nextStatesIdxs = [s.idx for s in state.nextStates]
    print("State: idx={} word={} isFinal={} nextStatesIdxs={} ftr={} ".format(
        state.idx, state.word, state.isFinal, nextStatesIdxs, state.ftr))


def load_graph(rxfilename):
    startState = State(None, 0)
    max_samples = 0

    graph = [startState, ]
    stateIdx = 1
    for word, features in FtrFile.FtrDirectoryReader(rxfilename):
        prevState = startState
        max_samples = max(max_samples, features.nSamples)

        for frame in range(features.nSamples):
            state = State(features.readvec(), stateIdx)
            state.nextStates.append(state)  # add loop
            prevState.nextStates.append(state)
            prevState = state
            graph.append(state)
            stateIdx += 1
        if state:
            state.word = word
            state.isFinal = True
    return graph, max_samples


def check_graph(graph):
    assert len(graph) > 0, "graph is empty."
    assert graph[0].ftr is None \
        and graph[0].word is None \
        and not graph[0].isFinal, "broken start state in graph."
    idx = 0
    for state in graph:
        assert state.idx == idx
        idx += 1
        assert (state.isFinal and state.word is not None) \
            or (not state.isFinal and state.word is None)


def print_graph(graph):
    print("*** DEBUG. GRAPH ***")
    np.set_printoptions(formatter={'float': '{: 0.1f}'.format})
    for state in graph:
        print_state(state)
    print("*** END DEBUG. GRAPH ***")


#########################################
# Token
#########################################

class Token:
    def __init__(self, state, dist=0.0, sentence=""):
        self.state = state
        self.dist = dist
        self.sentence = sentence


def print_token(token):
    print("Token on state #{} dist={} sentence={} word={}".format(token.state.idx,
                                                          token.dist,
                                                          token.sentence,
                                                          token.state.word))


def print_tokens(tokens):
    print("*** DEBUG. TOKENS LIST ***")
    for _, token in tokens.items():
        print_token(token)
    print("*** END DEBUG. TOKENS LIST ***")


def print_result_tokens_to_file(filename, final_tokens, winner_token, file_with_results):
    """ Print to file result all active tokens and the winner token """

    res_out_file = open(file_with_results, 'a')
    res_out_file.write('#' * 33 + '\n')
    res_out_file.write('#' * 10 + '  ' + filename + '  ' + '#' * 10 + '\n')
    res_out_file.write('#' * 33 + '\n')

    ''' Write all active tokens to file '''
    for _token in final_tokens:
        res_out_file.write('State: ' + str(_token.state.idx) +
                           ' dist: ' + str(_token.dist) +
                           ' word: ' + _token.state.word + '\n')

    ''' Write the Winner token '''
    res_out_file.write('Winner token :\n')
    res_out_file.write('State: ' + str(winner_token.state.idx) +
                       ' dist: ' + str(winner_token.dist) +
                       ' word: ' + winner_token.state.word + '\n')


def print_result_for_kaldi_wer(filename, winner_token, file_with_results):

    res_out_file = open(file_with_results, 'a')

    res_out_file.write(filename + ' ' + winner_token.state.word.split('_')[0] + '\n')

#########################################
# Decoder
#########################################


def recognize_step(next_tokens, ftr):
    """ One step for token passing algorithm """

    ''' Set nextTokens as active tokens. Clear nextTokens  '''
    active_tokens = next_tokens
    next_tokens = dict()

    for _, _token in active_tokens.items():

        ''' Generate all possible new tokens '''
        for _next_state in _token.state.nextStates:

            ''' Calc distance between state and new ftr '''
            new_dist = _token.dist + euclidean_distance(ftr, _next_state.ftr)

            ''' Create a new token '''
            new_token = Token(_next_state, new_dist)

            ''' Create new token '''
            token_in_state = next_tokens.get(new_token.state.idx, None)

            ''' Don't add new token If a token in state has less dist '''
            if token_in_state and token_in_state.dist < new_token.dist:
                continue

            ''' Add/replace new token for the next step '''
            next_tokens[new_token.state.idx] = new_token

    return next_tokens

def recognize( data_to_recognize, graph, max_samples, file_with_results, file_for_wer):
    filename = data_to_recognize[0]
    features = data_to_recognize[1]

    start_state = graph[0]
    # nextTokens = [Token(startState), ]
    next_tokens = {start_state.idx: Token(start_state)}

    last_idx = 0
    # for _idx in range(features.nSamples):
    while last_idx < max_samples:

        ''' Get next feature vector '''
        if last_idx < features.nSamples:
            _ftr = features.readvec()

        # print('#' * 30)
        # print_tokens(activeTokens)
        # print('Nsampl: ', features.nSamples, ' cur: ', last_idx, ' len: ', len(next_tokens))

        next_tokens = recognize_step(next_tokens, _ftr)
        last_idx += 1

    ''' Get result '''
    # print_tokens(next_tokens)

    # print(len(nextTokens))

    final_tokens = []
    for _, _token in next_tokens.items():
        if _token.state.isFinal:
            final_tokens.append(_token)

    distance = [_token.dist for _token in final_tokens]

    print('\n' + '#' * 30)
    print("Recognizing file '{}', samples={}".format(filename,
                                                     features.nSamples))

    if len(final_tokens) == 0:
        print('Not final tokens found')
    else:
        print('---------- Winner ----------')
        winner_token = final_tokens[distance.index(min(distance))]
        print_token(winner_token)

        print_result_tokens_to_file(filename, final_tokens, winner_token, file_with_results)
        print_result_for_kaldi_wer(filename, winner_token, file_for_wer)

#########################################
# Main
#########################################

from multiprocessing import Pool
from functools import partial

threads_count = 8

BASE_PATH = '/home/alex/projects/pycharm/ASRlesson030'


def base_recognition(etalons, records, result_file_names):

    graph, max_samples = load_graph(etalons)
    check_graph(graph)
    # print_graph(graph)

    ''' Remove old result files '''
    result_filename = result_file_names['result_filename']
    if os.path.exists(result_filename):
        os.remove(result_filename)
    file_for_wer = result_file_names['file_for_wer']
    if os.path.exists(file_for_wer):
        os.remove(file_for_wer)
    file_wer_train = result_file_names['file_wer_train']
    if os.path.exists(file_wer_train):
        os.remove(file_wer_train)

    ''' Set thread count for parallel calculation '''
    p = Pool(threads_count)
    sys.setrecursionlimit(10000)

    ''' Create train file for compute-wer '''
    train_file = open(file_wer_train, 'a')

    ''' Create initial data in vectors to run recognizers in parallel '''
    to_recognize_data = []
    for filename, features in FtrFile.FtrDirectoryReader(records):
        to_recognize_data.append((filename, features))
        train_file.write(filename + ' ' + filename.split('_')[0] + '\n')

        # recognize(filename, features, graph, max_samples, result_filename)

    ''' Run recognition '''
    p.map(partial(recognize, graph=graph, max_samples=max_samples,
                  file_with_results=result_filename, file_for_wer=file_for_wer),
          to_recognize_data)


def da_net_recognize():

    etalons = 'ark,t:' + BASE_PATH + '/data/da_net_etalons.txtftr'
    records = 'ark,t:' + BASE_PATH + '/data/da_net_test.txtftr'

    base_recognition(etalons, records,
                     {'result_filename': 'result.txt',
                      'file_for_wer': 'wer_results.txt',
                      'file_wer_train': 'wer_train.txt'}
                     )


def digits_recognition():
    etalons = 'ark,t:' + BASE_PATH + '/data/digits_etalons.txtftr'
    records = 'ark,t:' + BASE_PATH + '/data/digits_test.txtftr'

    base_recognition(etalons, records,
                     {'result_filename': 'digit_result.txt',
                      'file_for_wer': 'digit_wer_results.txt',
                      'file_wer_train': 'digit_wer_train.txt'}
                     )


def test_with_simple_file():

    etalons = 'ark,t:' + BASE_PATH + '/data/da_net.txtftr'
    records = 'ark,t:' + BASE_PATH + '/data/record.txtftr'

    graph, max_samples = load_graph(etalons)
    check_graph(graph)
    print_graph(graph)

    for filename, features in FtrFile.FtrDirectoryReader(records):
        recognize([filename, features], graph, max_samples,
                  'simple_result.txt', 'simple_wer.txt')


if __name__ == "__main__":

    # da_net_recognize()

    # digits_recognition()

    test_with_simple_file()
